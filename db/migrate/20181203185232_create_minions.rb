class CreateMinions < ActiveRecord::Migration[5.1]
  def change
    create_table :minions do |t|
      t.string :name
      t.string :description
      t.string :price

      t.timestamps
    end
  end
end

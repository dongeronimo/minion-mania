# MINION MANIA
Uma app onde as pessoas podem comprar minions.
## Instalação
1) Assumindo que a máquina tenha ruby on rails instalado e configurado, $``` git clone https://bitbucket.org/dongeronimo/minion-mania/src/master/```
2) Depois da clonagem do repositório, entrar no diretório do repositório e puxar as dependências: $```bundle install --without production```. Ao menos que você esteja na produção, aí é $```bundle install```. A diferença é no desenvolvimento usa SQLite e na produção Postgres.
3) Depois de puxado, atualizar o banco local: $```rails db:migrate```
3) Depois de ter atualizado o banco, executar $```rails test``` para verificar se está tudo ok.
4) Se tudo ok, iniciar o servidor com $```rails server`´`

## Criação de novos minions
Em /minion_form/form existe uma tela de criação de minions. No momento ela está aberta para 
qualquer um usar. No futuro ela teria que ter uma barreira de usuário.
## Uma versão na produção
No heroku (https://minion-mania.herokuapp.com/) há uma versão rodando. No heroku o banco é o postgres.
## Etapas
### Fazer
6. Criar a entidade Cliente
7. Fazer o processo de login
8. Fazer o processo de novo cliente
9. Criar a entidade minion
12. Busca de minions
13. Tela de detalhamento de minions
14. Entidade carrinho de compras
15. Adicionar minion ao carrinho
16. Se o user não for cliente, exigir login. Se não tiver login o cara vai ter que fazer singin
17. Conclusão de compra.
18. O carrinho de compra deve sobreviver mesmo com f5

### Fazendo
10. Criar uns minions
11. Criar a listagem de minions na tela inicial (o card com os dados básicos do minion)

### Feito
1. Criar o esqueleto do projeto
2. Criar a entidade User
3. Criar o esqueleto da página inicial
4. Ao entrar na página inicial criar um objeto User
5. Fazer o layout da página inicial


## Editado com  [dillinger.io](https://dillinger.io/) 
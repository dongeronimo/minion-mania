require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new()
  end
  
  test "must have an UID" do
    @user.uid = "   "
    assert_not @user.valid?
  end
  
  test "instantiated with UID" do
    @user = User.new()
  end
end

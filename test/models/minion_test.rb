require 'test_helper'

class MinionTest < ActiveSupport::TestCase
  def setup
    @minion = Minion.new
  end
  
  test "must have name" do
    @minion.name = "   "
    @minion.description = "foobar"
    @minion.price = "10.00"
    assert_not @minion.valid?
  end
  
  test "must have description" do
    @minion.name = "foobar"
    @minion.description = "  "
    @minion.price = "10.00"
    assert_not @minion.valid?
  end
  
  test "must have price" do
    @minion.name = "foobar"
    @minion.description = "bazwoo"
    @minion.price = "  "
    assert_not @minion.valid?
  end
  
  test "price must be float" do
    begin
      @minion.name = "foobar"
      @minion.description = "bazwoo"
      @minion.price = "  "
      fPrice = Float(@minion.price)
    rescue
      assert true
      return
    end
    assert false
  end
  
  test "can save minion?" do
    @minion = Minion.new
    @minion.name = "Teste"
    @minion.description = "ESSE É UM MINION DE TESTE"
    @minion.price = "100.00"
    assert @minion.valid?
    @minion.save
    assert @minion.id?
    @minion.destroy
  end
end

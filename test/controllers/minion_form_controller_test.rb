require 'test_helper'

class MinionFormControllerTest < ActionDispatch::IntegrationTest
#  test "should get form" do
#    get minion_form_form_url
#    assert_response :success
#  end

  test "must reject wrong minion" do
    data = {name: "", description: "  ", price: ""}.to_json
    post minion_index_path, params: data
    assert_response :bad_request
  end

  test "can save new minion?" do
    data = {name: Time.now.getutc.to_s, description: "Loren ipsun", price: "10.00"}.to_json
    post minion_index_path, params: data
    assert_response :success
    jsonResposta = JSON.parse response.parsed_body.to_json
    assert_not jsonResposta["id"].blank?
  end
end

require 'test_helper'

class UserControllerTest < ActionDispatch::IntegrationTest
  test "user creation" do
    get new_user_path
    assert_response :success
    jsonResposta = JSON.parse response.parsed_body.to_json
    assert_not jsonResposta["uid"].blank?
  end
  

end

require 'test_helper'

class MinionControllerTest < ActionDispatch::IntegrationTest
  test "minion find all" do
    get minion_index_path
    assert_response :success
    jsonResposta = JSON.parse response.parsed_body.to_json
    numberOfEntriesInReply = jsonResposta.count
    numberOfEntriesInDb = listOfMinionsInDb = Minion.all.count
    
    assert numberOfEntriesInReply == numberOfEntriesInDb
  end
end

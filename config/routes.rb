Rails.application.routes.draw do
  #get  '/help',    to: 'static_pages#help'
  get '/new_minion_form', to: 'minion_form#form'
  #get 'minion_form/form'

  get 'minion_mania/home'

  resources :minion
  resources :user

  root 'minion_mania#home'

end

#pra cada json de minion, construir um card e por na div dada
window.buildMinionCards = (listOfMinions, targetDiv)->
  $("#minion_container").empty() #preciso esvaizar o container
  listOfMinions.forEach (currentMinion, index)->
    $base = $("#minionCardBase") #a minha template
    $cloned = $base.clone()
    $cloned.attr = id:"minion_card_"+currentMinion.id#template clonada e com id modificado
    $cloned.find(".nameDiv").text(currentMinion.name)
    $cloned.find(".priceDivText").text("$ "+currentMinion.price)

    $cloned.find(".btn.btn-primary")[0].onclick=()->
      console.log "add to cart "+currentMinion.id
      return
    $cloned.find(".btn.btn-secondary")[0].onclick=()->
      console.log "remove from cart "+currentMinion.id
      return
      
    targetDiv.append($cloned)
    return
  return
#Quando a página é carregada eu devo pedir pra criar um novo usuário se já não tem
#usuário na window
window.updateMinions = ()->
  fetch '/minion/'
    .then (response) ->
      response.json()
    .then (listOfMinions) ->
      console.log (listOfMinions)
      window.buildMinionCards(listOfMinions, $("#minion_container"))
      return
  return

#Quando eu to voltando, devo atualizar a lista de minions
window.addEventListener 'popstate', (event)->
  window.updateMinions()
  return

window.onload = () ->
  #TODO usar cookies pra guardar o user pra n perder o carrinho mesmo com f5
  #ou usar localstorage.
  if window.user == undefined
    console.log "n tem user, pede um"
    requestData = method: 'get'
    fetch '/user/new/', requestData
    .then (response) ->
      response.json()
    .then (myJson)->
      window.user = myJson
      window.updateMinions()
    .catch (error)->
      console.error(error)
  else
    console.log "tem"
  return

window.saveNewMinion = () ->
  _name = $("#minion_form_name").val()
  _description = $("#minion_form_description").val()
  _price = $("#minion_form_price").val()
  data = {name: _name, description: _description, price: _price}
  
  requestData = method: 'post', body: JSON.stringify(data)
  fetch '/minion/', requestData
    .then (response)->
      if not response.ok
        throw response
      response.json()
    .then (myJson)->
      #TODO: QUANDO GRAVAR UM MINION DAR UMA RESPOSTA NA TELA
      alert "gravado com sucesso"
      #TODO: LIMPAR ERROS E CAMPOS
      $("#new_minion_errors").empty()
      $("#minion_form_name").val("")
      $("#minion_form_description").val("")
      $("#minion_form_price").val("")
      return
    .catch (error)->
      #TODO: NAO SEI RETORNAR JSON COM ERRO. VAI TER QUE FICAR PRA DEPOIS
      alert "Erro! confira os dados"
      return
  return
  
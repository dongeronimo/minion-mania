class MinionController < ApplicationController
  def index
    listOfMinions = Minion.all
    render json: listOfMinions.to_json, status: :ok
  end

  def show
  end

  def new
  end
  #grava o minion a partir do json vindo do cliente. Se os dados estiverem 
  #incompletos dá um erro pro cliente;
  def create
    receivedObject = JSON.parse(request.body.read)
    @minion = Minion.new
    @minion.name = receivedObject["name"]
    @minion.description = receivedObject["description"]
    @minion.price = receivedObject["price"]
    if @minion.valid?
      @minion.save
      render json: @minion, status: :ok
    else
      err = @minion.errors.messages
      @errJson = {
        status: 400,
        code: "foobar",
        title: "loren ipsun?",
        detail: "babdjsabdnbasnbdnasb"
      }
      render json: @errJson, status: 400
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end
end

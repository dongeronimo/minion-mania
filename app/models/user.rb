class User < ApplicationRecord
  #validates :name, presence: true , length:{maximum: 50}
  validates :uid, presence: true
  
    def initialize()
        super()
        rnd = ('a'..'z').to_a.shuffle[0..14].join
        self.uid = rnd
    end
end
